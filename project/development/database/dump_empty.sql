/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.23 : Database - samplezf2wd2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`samplezf2wd2` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `samplezf2wd2`;

/*Table structure for table `books` */

DROP TABLE IF EXISTS `books`;

CREATE TABLE `books` (
  `b_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_name` varchar(255) NOT NULL,
  `b_published_year` int(11) NOT NULL,
  `b_active` tinyint(1) NOT NULL DEFAULT '1',
  `b_created_at` datetime DEFAULT NULL,
  `b_updated_at` datetime DEFAULT NULL,
  `b_price` float(10,2) NOT NULL DEFAULT '0.00',
  `b_author` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`b_id`),
  KEY `i_b_active` (`b_active`),
  KEY `i_b_name` (`b_name`),
  KEY `i_b_price` (`b_price`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `books` */

LOCK TABLES `books` WRITE;

insert  into `books`(`b_id`,`b_name`,`b_published_year`,`b_active`,`b_created_at`,`b_updated_at`,`b_price`,`b_author`) values (1,'cxvxc',2004,1,'2014-11-24 19:56:56','2014-11-25 16:10:26',65.33,'fdsfsdf'),(2,'ываыва',3333,1,'2014-11-25 15:41:29','2014-11-25 16:10:38',0.00,'sdsfsd'),(3,'ывавыа',1111,1,'2014-11-25 15:42:34','2014-11-25 16:10:48',0.00,'vddffdsfds'),(4,'ываыва',2001,1,'2014-11-25 15:43:45',NULL,44.00,'ываыва'),(5,'name',2014,1,'2014-11-25 15:44:22',NULL,11.00,'author'),(6,'dsfds',2222,1,'2014-11-25 15:46:33',NULL,22.00,'sdfdsf'),(7,'sdfsd',3333,1,'2014-11-25 15:47:13',NULL,333.00,'dsfds'),(8,'dsfds',2222,1,'2014-11-25 15:47:52','2014-11-25 15:50:21',33.00,'sdfdsf');

UNLOCK TABLES;

/*Table structure for table `links_publishing_house_books` */

DROP TABLE IF EXISTS `links_publishing_house_books`;

CREATE TABLE `links_publishing_house_books` (
  `lphb_phb_id` int(11) NOT NULL,
  `lphb_b_id` int(11) NOT NULL,
  PRIMARY KEY (`lphb_phb_id`,`lphb_b_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `links_publishing_house_books` */

LOCK TABLES `links_publishing_house_books` WRITE;

insert  into `links_publishing_house_books`(`lphb_phb_id`,`lphb_b_id`) values (1,2),(1,3),(1,4),(1,6),(1,7),(2,2),(2,3),(2,4),(2,5),(2,6),(2,7),(2,8),(3,1),(3,2),(3,3),(3,5);

UNLOCK TABLES;

/*Table structure for table `publishing_house` */

DROP TABLE IF EXISTS `publishing_house`;

CREATE TABLE `publishing_house` (
  `pbh_id` int(11) NOT NULL AUTO_INCREMENT,
  `pbh_name` varchar(255) NOT NULL,
  `pbh_active` tinyint(1) NOT NULL DEFAULT '1',
  `pbh_created_at` datetime DEFAULT NULL,
  `pbh_updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pbh_id`),
  KEY `i_pbh_name` (`pbh_name`),
  KEY `i_pbh_active` (`pbh_active`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `publishing_house` */

LOCK TABLES `publishing_house` WRITE;

insert  into `publishing_house`(`pbh_id`,`pbh_name`,`pbh_active`,`pbh_created_at`,`pbh_updated_at`) values (1,'Издательство 1',1,'2014-11-25 14:45:21','2014-11-25 14:53:33'),(2,'Издательство 2',1,'2014-11-25 14:47:59','2014-11-25 14:53:48'),(3,'Издательство 3',1,'2014-11-25 14:48:34','2014-11-25 14:53:57');

UNLOCK TABLES;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_name` varchar(255) NOT NULL,
  `u_passwd` varchar(255) NOT NULL,
  `u_role_id` int(11) DEFAULT '0',
  `u_st` varchar(255) NOT NULL,
  `u_created_at` datetime DEFAULT NULL,
  `u_updated_at` datetime DEFAULT NULL,
  `u_active` tinyint(1) DEFAULT '1',
  `u_link_restore` varchar(255) DEFAULT NULL,
  `u_datetime_restore` datetime DEFAULT NULL,
  `u_code_active` varchar(255) DEFAULT NULL,
  `u_not_remove` tinyint(1) DEFAULT '0',
  `u_not_edit` tinyint(1) DEFAULT '0',
  `u_auth_code` varchar(255) DEFAULT NULL,
  `u_cp_link` varchar(255) DEFAULT NULL,
  `u_restore_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`u_id`),
  KEY `i_name` (`u_name`),
  KEY `i_role_id` (`u_role_id`),
  KEY `i_created_at` (`u_created_at`),
  KEY `i_updated_at` (`u_updated_at`),
  KEY `i_u_code_active` (`u_code_active`),
  KEY `i_u_not_remove` (`u_not_remove`),
  KEY `i_u_not_edit` (`u_not_edit`),
  KEY `i_u_auth_code` (`u_auth_code`),
  KEY `i_u_cp_link` (`u_cp_link`),
  KEY `i_u_restore_link` (`u_restore_link`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

LOCK TABLES `users` WRITE;

insert  into `users`(`u_id`,`u_name`,`u_passwd`,`u_role_id`,`u_st`,`u_created_at`,`u_updated_at`,`u_active`,`u_link_restore`,`u_datetime_restore`,`u_code_active`,`u_not_remove`,`u_not_edit`,`u_auth_code`,`u_cp_link`,`u_restore_link`) values (1,'admin@samplezf2wd2.levio.pro','6e91855152d58b236c7c739339e17c13',1,'0d6a268c094a3993a96689041c9580f6','2013-05-22 13:39:27','2014-03-26 13:47:21',1,NULL,NULL,NULL,1,0,NULL,NULL,'');

UNLOCK TABLES;

/*Table structure for table `users_role` */

DROP TABLE IF EXISTS `users_role`;

CREATE TABLE `users_role` (
  `ur_id` int(11) NOT NULL AUTO_INCREMENT,
  `ur_name` varchar(255) NOT NULL DEFAULT 'director',
  `ur_description` text,
  `ur_created_at` datetime DEFAULT NULL,
  `ur_udated_at` datetime DEFAULT NULL,
  `ur_ct_change` tinyint(1) DEFAULT '0',
  `ur_ct_edit` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`ur_id`),
  KEY `i_ur_name` (`ur_name`),
  KEY `i_ur_created_at` (`ur_created_at`),
  KEY `i_ur_updated_at` (`ur_udated_at`),
  KEY `i_ur_ct_change` (`ur_ct_change`),
  KEY `i_ur_ct_edit` (`ur_ct_edit`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `users_role` */

LOCK TABLES `users_role` WRITE;

insert  into `users_role`(`ur_id`,`ur_name`,`ur_description`,`ur_created_at`,`ur_udated_at`,`ur_ct_change`,`ur_ct_edit`) values (1,'administrator',NULL,'2014-11-01 13:39:27','2014-11-01 13:39:27',0,0);

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
