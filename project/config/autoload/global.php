<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
define('LIVE_HOST','none-samplezf2wd2.levio.pro');

define( 'DEF_JS_VER', '1' );
define( 'DEF_CSS_VER', '1' );

define( 'DEF_NUMBER_IN_MAP', '');

define( 'DEF_COUNT_PAGINATION', 25 );
define( 'DEF_MIDDLE_COUNT_PAGINATION', 15 );
define( 'DEF_SMALL_COUNT_PAGINATION', 10 );



define( 'DEF_DATE_MASK', 'd-m-Y ' );
define( 'DEF_DATE_MASK_ADMIN', 'Y-m-d' );
define( 'DEF_TIME_MASK_ADMIN', 'H:i' );

define( 'DEF_ADMIN_NAME_ROLE', 'administrator' );
define( 'DEF_LOGIN_USER_NAME_ROLE', 'user' );
define( 'DEF_LOGIN_ANONYMOUS_NAME_ROLE', 'anonymous' );
define( 'DEF_USER_NAME_ROLE', 'anonymous' );

define( 'DEF_DEBUGGING_FOR_ADMIN', true);

define( 'DEF_MSG_SUCCESS_OBJECT_IS_SAVED', 'Данные сохранены');
define( 'DEF_MSG_ERROR_FILE_NOT_FOUND', 'Файл не найден');

return array(
    'db' => array(
        'driver'         => 'Pdo',
        'dsn'            => 'mysql:dbname=samplezf2wd2;host=localhost',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'ZendDbAdapterAdapter'
                                                 => 'Zend\Db\Adapter\AdapterServiceFactory',
            'Zend\Cache\StorageFactoryFile' => function() {
                return Zend\Cache\StorageFactory::factory(
                    array(
                        'options' => array(
                            //TODO: change time for live
                            //'ttl' => 86400,
                            'ttl' => 200,
                        ),
                        'adapter' => array(
                            'name' => 'filesystem',
                            'options' => array(
                                'dirLevel' => 2,
                                'cacheDir' => 'data/cache',
                                'dirPermission' => 0755,
                                'filePermission' => 0666,
                                'namespaceSeparator' => '-filesx-'
                            ),
                        ),
                        'plugins' => array('serializer'),
                    )
                );
            },
            'Zend\Cache\StorageFactoryRedis' => function() {
                return Zend\Cache\StorageFactory::factory(
                    array(
                        'adapter' => array (
                            'name' => 'redis',
                            'options' => array (
                                'server' => array(
                                    'host' => 'localhost',
                                    'port' => 6379,

                                ),
                                'ttl' => '172800'
                            )
                        ),
                    )
                );
            },
            'Zend\Cache\StorageFactoryRedisLong' => function() {
                return Zend\Cache\StorageFactory::factory(
                    array(
                        'adapter' => array (
                            'name' => 'redis-long',
                            'options' => array (
                                'server' => array(
                                    'host' => 'localhost',
                                    'port' => 6379,

                                ),
                                'ttl' => '172800'
                            )
                        ),
                    )
                );
            },

        ),
        'aliases' => array(
            'cache' => 'Zend\Cache\StorageFactoryFile',
            'redis' => 'Zend\Cache\StorageFactoryRedis',
            'redis-long' => 'Zend\Cache\StorageFactoryRedisLong',
        ),

    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => dirname( __FILE__ ) . '/../../module/Application/view/layout/layout_books.phtml',
            'layout/index'           => dirname( __FILE__ ) . '/../../module/Application/view/layout/layout_books.phtml',
            'layout/admin'           => dirname( __FILE__ ) . '/../../module/Application/view/layout/layout_c_admin.phtml',
            'layout/login'           => dirname( __FILE__ ) . '/../../module/Application/view/layout/layout_login.phtml',
            'layout/header'           => dirname( __FILE__ ) . '/../../module/Application/view/partial/header.phtml',
            'layout/notifications_admin'           => dirname( __FILE__ ) . '/../../module/Application/view/partial/notifications_admin.phtml',


            'application/index/index' => dirname( __FILE__ ) . '/../../module/Application/view/application/index/index.phtml',
            'error/index'             => dirname( __FILE__ ) . '/../../module/Application/view/error/index.phtml',
            'admin/messages'    => dirname( __FILE__ ) . '/../../module/Application/view/partial/admin/messages.phtml',
            'admin/pagination'    => dirname( __FILE__ ) . '/../../module/Application/view/partial/admin/pagination.phtml',

        ),

    ),

);
