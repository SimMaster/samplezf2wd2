<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Book\Controller\Index' => 'Book\Controller\IndexController',
            'Book\Controller\Admin' => 'Book\Controller\AdminController',

        ),
    ),

    'router' => array(
        'routes' => array(
            'admin-book' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin-book[/]',

                    'defaults' => array(
                        'controller' => 'Book\Controller\Admin',
                        'action'     => 'index',
                    ),
                ),
            ),

            'admin-add-ph' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin-get-ph[/][:id][/]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Book\Controller\Admin',
                        'action'     => 'get-ph',
                    ),
                ),
            ),

            'admin-add-book' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin-get-book[/][:id][/]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Book\Controller\Admin',
                        'action'     => 'get-book',
                    ),
                ),
            ),

        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'book' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy'
        ),
    )
);