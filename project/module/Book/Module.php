<?php
namespace Book;


use Contest\Model\ContestTable;

use Prize\Model\PrizeItem;
use Prize\Model\PrizeTable;
use Topic\Model\TopicItem;
use Topic\Model\TopicTable;
use Zend\Db\ResultSet\ResultSet;

use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\ModuleManager;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function init ( ModuleManager $manager )
    {
        $events = $manager->getEventManager();
        $sharedEvents = $events->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', function($e) {
                $controller = $e->getTarget();
                $routeMatch = $e->getRouteMatch();
                $controllerName = strtolower(end(explode('\\', $routeMatch->getParam('controller'))));

                $routeName = $routeMatch->getMatchedRouteName();
                $controller->layout('layout/'.$controllerName);
            }, 100);
    }

    /**
     * @return array
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Book\Model\TopicTableOut' =>  'Book\Factory\Model\BookTableFactory',

                'Book\Model\BookTable' =>  function($sm) {
                        $tableGateway = $sm->get('BookTableGateway');
                        $table = new TopicTable( $tableGateway );
                        return $table;
                    },
                'TopicItemTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype( new TopicItem() );

                        return new TableGateway( 'topics', $dbAdapter, null, $resultSetPrototype);
                    },


            ),
        );
    }
}
