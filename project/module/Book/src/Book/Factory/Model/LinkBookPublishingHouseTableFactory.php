<?php

namespace Book\Factory\Model;




use Book\Model\LinkBookPublishingHouseItem;
use Book\Model\LinkBookPublishingHouseTable;
use Zend\Di\ServiceLocator;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;

use Zend\Stdlib\Hydrator\ObjectProperty;
use Zend\Db\ResultSet\HydratingResultSet;

class LinkBookPublishingHouseTableFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $db = $serviceLocator->get('Zend\Db\Adapter\Adapter');

        $resultSetPrototype = new HydratingResultSet();
        $resultSetPrototype->setHydrator(new ObjectProperty());
        $resultSetPrototype->setObjectPrototype(new LinkBookPublishingHouseItem());

        $tableGateway       = new TableGateway('links_publishing_house_books', $db, null, $resultSetPrototype);
        $table              = new LinkBookPublishingHouseTable($tableGateway);

        return $table;
    }

}

