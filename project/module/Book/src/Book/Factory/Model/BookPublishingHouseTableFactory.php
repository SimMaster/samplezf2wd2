<?php

namespace Book\Factory\Model;



use Book\Model\BookPublishingHouseItem;
use Book\Model\BookPublishingHouseTable;
use Zend\Di\ServiceLocator;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;

use Zend\Stdlib\Hydrator\ObjectProperty;
use Zend\Db\ResultSet\HydratingResultSet;

class BookPublishingHouseTableFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $db = $serviceLocator->get('Zend\Db\Adapter\Adapter');

        $resultSetPrototype = new HydratingResultSet();
        $resultSetPrototype->setHydrator(new ObjectProperty());
        $resultSetPrototype->setObjectPrototype(new BookPublishingHouseItem());

        $tableGateway       = new TableGateway('publishing_house', $db, null, $resultSetPrototype);
        $table              = new BookPublishingHouseTable($tableGateway);

        return $table;
    }

}

