<?php


namespace Book\Controller;

use Accounts\Factory\Model\UsersTableFactory;
use Accounts\Model\User;

use Book\Factory\Model\BookPublishingHouseTableFactory;
use Book\Factory\Model\BookTableFactory;
use Book\Factory\Model\LinkBookPublishingHouseTableFactory;
use Book\Model\BookItem;
use Book\Model\BookPublishingHouseItem;
use Book\Model\BookPublishingHouseTable;
use Book\Model\LinkBookPublishingHouseItem;
use Messages\Factory\Model\MessagesStackTableFactory;
use Messages\Model\MessagesItem;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;


use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;


class AdminController extends AbstractActionController
{

    protected $viewModel;
    protected $serviceLocator;

    protected $acceptMapping
        = array(
            'Zend\View\Model\ViewModel' => array(
                'text/html'
            ),
            'Zend\View\Model\JsonModel' => array(
                'application/json'
            )
        );

    protected $userAuth;

    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = $this->acceptableViewModelSelector($this->acceptMapping);
        $this->serviceLocator = $this->getServiceLocator();

        $sessionAuth = new Container('userAuth');
        $this->userAuth = (object)$sessionAuth->user_store;

        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        $showError = false;
        $success = false;
        $arrErrors = array();
        $token = "";

        $listBooks = array();
        $listPH = array();

        $fctBooksTable = new BookTableFactory();
        $tblBooks = $fctBooksTable->createService( $this->serviceLocator );
        $listBooks = $tblBooks->getList();
        if( $listBooks ) {
            $fctLinkBooksPH = new LinkBookPublishingHouseTableFactory();
            $tblLinksBooksPH = $fctLinkBooksPH->createService( $this->serviceLocator );

            $fctPHTable = new BookPublishingHouseTableFactory();
            $tblPH = $fctPHTable->createService( $this->serviceLocator );
            $listBooks = $listBooks->toArray();
            foreach( $listBooks as $keyIndex => $objBook ) {
                $selectedPH = array();
                $selectedPH = $tblLinksBooksPH->getListByBook($objBook["b_id"]);

                $listInPH = array();
                if ( count($selectedPH) ) {
                    $listInPH = $tblPH->getListByArray($selectedPH);
                    if($listInPH) {
                        $listInPH = $listInPH->toArray();
                    }

                }

                $listBooks[$keyIndex]["listPH"] = $listInPH;

            }
        }



        $fctPHTable = new BookPublishingHouseTableFactory();
        $tblPH = $fctPHTable->createService( $this->serviceLocator );
        $listPH = $tblPH->getList();


        $arrAnswer['success'] =  $success;
        $arrAnswer['showError'] = $showError;
        $arrAnswer['errors'] = $arrErrors;
        $arrAnswer['token'] = $token;
        $arrAnswer['listBooks'] = $listBooks;
        $arrAnswer['listPH'] = $listPH;



        $this->viewModel->setVariables(
            $arrAnswer
        );

        return $this->viewModel;

    }

    public function getPhAction()
    {
        $showError = false;
        $success = false;
        $arrErrors = array();
        $token = "";
        $objPH = false;

        $arrAnswer = array();

        $phId = (int)$this->params()->fromRoute('id', null);
        if( $phId != 0 ) {
            $fctPHTable = new BookPublishingHouseTableFactory();
            $tblPH = $fctPHTable->createService( $this->serviceLocator );
            $objPH = $tblPH->getObject( $phId );
        }

        if( $this->getRequest()->isPost() ) {
            $fctPHTable = new BookPublishingHouseTableFactory();
            $tblPH = $fctPHTable->createService( $this->serviceLocator );
            $dataForm = $this->params()->fromPost('ph');
            $dataOut = array();
            foreach( $dataForm as $keyString => $strValue ) {
                $dataOut[('pbh_' . $keyString)] = strip_tags($strValue);
            }
            $objPH = new BookPublishingHouseItem();
            $objPH->exchangeArray( $dataOut );
            $answerAction = $tblPH->saveConfiguration( $objPH );
            if( $answerAction == false ) {
                $arrAnswer['objPH'] = $objPH;
                $arrAnswer['showError'] = true;
                $this->viewModel
                    ->setTerminal(true);
                $this->viewModel->setVariables(
                    $arrAnswer
                );
            } else {
                $success = true;
            }

        }




        $arrAnswer['success'] =  $success;
        $arrAnswer['showError'] = $showError;
        $arrAnswer['errors'] = $arrErrors;
        $arrAnswer['token'] = $token;
        $arrAnswer['objPH'] = $objPH;


        $this->viewModel
            ->setTerminal(true);
        $this->viewModel->setVariables(
            $arrAnswer
        );

        return $this->viewModel;
    }

    public function getBookAction()
    {
        $showError = false;
        $success = false;
        $arrErrors = array();
        $token = "";
        $objBook = false;
        $selectedPH = array();

        $listPH = array();
        $fctPHTable = new BookPublishingHouseTableFactory();
        $tblPH = $fctPHTable->createService( $this->serviceLocator );
        $listPH = $tblPH->getList();

        $bookId = (int)$this->params()->fromRoute('id', null);
        if( $bookId != 0 ) {
            $fctBooksTable = new BookTableFactory();
            $tblBooks = $fctBooksTable->createService( $this->serviceLocator );
            $objBook = $tblBooks->getObject( $bookId );
            $fctLinkBooksPH = new LinkBookPublishingHouseTableFactory();
            $tblLinksBooksPH = $fctLinkBooksPH->createService( $this->serviceLocator );
            $selectedPH = $tblLinksBooksPH->getListByBook($bookId);

        }

        if( $this->getRequest()->isPost() ) {
            $fctBooksTable = new BookTableFactory();
            $tblBooks = $fctBooksTable->createService( $this->serviceLocator );
            $dataForm = $this->params()->fromPost('book');
            $dataOut = array();
            foreach( $dataForm as $keyString => $strValue ) {
                $dataOut[('b_' . $keyString)] = strip_tags($strValue);
            }
            $objBook = new BookItem();
            $objBook->exchangeArray( $dataOut );
            $answerAction = $tblBooks->saveConfiguration( $objBook );
            if( $answerAction == false ) {
                $arrAnswer['objPH'] = $objBook;
                $arrAnswer['showError'] = true;
                $this->viewModel
                    ->setTerminal(true);
                $this->viewModel->setVariables(
                    $arrAnswer
                );
            } else {
                $fctLinkBooksPH = new LinkBookPublishingHouseTableFactory();
                $tblLinksBooksPH = $fctLinkBooksPH->createService( $this->serviceLocator );
                $tblLinksBooksPH->deleteObjectByBookId($answerAction);
                $dataForm = $this->params()->fromPost('ph_select');
                $dataOut = array();
                foreach( $dataForm as $keyString => $strValue ) {
                    $objBookLink = new LinkBookPublishingHouseItem();
                    $dataOut = array('lphb_phb_id' => strip_tags($strValue), 'lphb_b_id' => $answerAction);
                    $objBookLink->exchangeArray( $dataOut );
                    $answerActionIn = $tblLinksBooksPH->saveConfiguration( $objBookLink );

                }
                $selectedPH = $tblLinksBooksPH->getListByBook($answerAction);
                $success = true;
            }

        }




        $arrAnswer['success'] =  $success;
        $arrAnswer['showError'] = $showError;
        $arrAnswer['errors'] = $arrErrors;
        $arrAnswer['token'] = $token;
        $arrAnswer['objBook'] = $objBook;
        $arrAnswer['listPH'] = $listPH;
        $arrAnswer['selectedPH'] = $selectedPH;

        $this->viewModel
            ->setTerminal(true);
        $this->viewModel->setVariables(
            $arrAnswer
        );

        return $this->viewModel;
    }









}

