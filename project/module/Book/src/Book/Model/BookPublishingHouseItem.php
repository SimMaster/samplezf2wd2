<?php

namespace Book\Model;

use Zend\Db\Sql\Expression;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class BookPublishingHouseItem  {


    public $pbh_id;
    public $pbh_name;

    public $pbh_active;

    public $pbh_created_at;
    public $pbh_updated_at;





    /**
     * @param array $data
     */
    public function exchangeArray( $data )
    {

        $this->pbh_id = (isset($data['pbh_id'])) ? $data['pbh_id'] : null;
        $this->pbh_name = (isset($data['pbh_name'])) ? $data['pbh_name'] : null;

        $this->pbh_active = (isset($data['pbh_active'])) ? $data['pbh_active'] : 1;

        $this->pbh_created_at = (isset($data['pbh_created_at'])) ? $data['pbh_created_at'] : new Expression('NOW()');
        $this->pbh_updated_at = (isset($data['pbh_updated_at'])) ? $data['pbh_updated_at'] : new Expression('NOW()');


    }

    /**
     * @param $nameVariable
     * @param $valueVariable
     */
    public function __set( $nameVariable, $valueVariable ) {
        $this->$nameVariable = $valueVariable;
    }

    /**
     * @param $nameVariable
     *
     * @return bool
     */
    public function __get( $nameVariable ) {
        return $this->$nameVariable ? $this->$nameVariable : false;
    }





}