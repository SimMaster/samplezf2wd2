<?php

namespace Book\Model;

use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Di\ServiceLocator;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Renderer\PhpRenderer;

class LinkBookPublishingHouseTable {

    /*================================================================================================================*/
    /**
     * Standard functions
     * TODO: move to base extend class
     */
    protected $tableGT;
    protected $rPrefix = "lphb";

    /**
     * @param TableGateway $tableGT
     */
    public function __construct( TableGateway $tableGT ) {
        $this->tableGT = $tableGT;
    }


    /**
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {

        $objSelect = new Select();
        $objSelect->from( $this->tableGT->table  )
        ;

        $resultSet = $this->tableGT->selectWith( $objSelect );
        return $resultSet;
    }



    /**
     * @param LinkBookPublishingHouseItem $object
     *
     * @return bool|int
     */
    public function saveConfiguration(LinkBookPublishingHouseItem $object)
    {
        $data = array(

            $this->rPrefix. '_phb_id' => $object->lphb_phb_id,
            $this->rPrefix. '_b_id' => $object->lphb_b_id,

        );

        return $this->tableGT->insert($data);


    }




    /*
     * End Standard Functions
     */
    /*================================================================================================================*/
    /*
     * Custom functions
     * */

    /**
     * @param int $b_id
     *
     * @return int
     */
    public function deleteObjectByBookId($b_id)
    {
        return $this->tableGT->delete(array($this->rPrefix. '_b_id' => $b_id));
    }

    /**
     * @param int $phb_id
     *
     * @return int
     */
    public function deleteObjectByPHId($phb_id)
    {
        return $this->tableGT->delete(array($this->rPrefix. '_phb_id' => $phb_id));
    }

    /**
     * @param $bookId
     *
     * @return array
     */
    public function getListByBook($bookId)
    {
        $objSelect = new Select();
        $objSelect->from( $this->tableGT->table  )
            ->columns(array('phbids' => $this->rPrefix . '_phb_id'), true)
            ->where($this->rPrefix. '_b_id = ' . $bookId)
        ;

        $resultSet = $this->tableGT->selectWith( $objSelect );
        $arrOut = array();

        if( $resultSet ) {
            foreach( $resultSet as $keyIndex => $objItem )
            {
                $arrOut[] = $objItem->phbids;
            }
        }
        return $arrOut;
    }

    /**
     * @param int $phId
     *
     * @return array
     */
    public function getListByPH($phId)
    {
        $objSelect = new Select();
        $objSelect->from( $this->tableGT->table  )
            ->columns(array('bids' => $this->rPrefix . '_b_id'), true)
            ->where($this->rPrefix. '_pbh_id = ' . $phId)
        ;

        $resultSet = $this->tableGT->selectWith( $objSelect );
        $arrOut = array();
        if( $resultSet ) {
            foreach( $resultSet as $keyIndex => $objItem )
            {
                $arrOut[] = $objItem['bids'];
            }
        }
        return $arrOut;
    }










}