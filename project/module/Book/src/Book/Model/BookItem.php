<?php

namespace Book\Model;

use Zend\Db\Sql\Expression;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class BookItem  {


    public $b_id;
    public $b_name;
    public $b_published_year;
    public $b_active;
    public $b_price;
    public $b_author;


    public $b_created_at;
    public $b_updated_at;




    /**
     * @param array $data
     */
    public function exchangeArray( $data )
    {

        $this->b_id = (isset($data['b_id'])) ? $data['b_id'] : null;
        $this->b_name = (isset($data['b_name'])) ? $data['b_name'] : null;
        $this->b_published_year = (isset($data['b_published_year'])) ? $data['b_published_year'] : date('Y');
        $this->b_price = (isset($data['b_price'])) ? $data['b_price'] : 0;
        $this->b_author = (isset($data['b_author'])) ? $data['b_author'] : "";

        $this->b_active = (isset($data['b_active'])) ? $data['b_active'] : 1;

        $this->b_created_at = (isset($data['b_created_at'])) ? $data['b_created_at'] : new Expression('NOW()');
        $this->b_updated_at = (isset($data['b_updated_at'])) ? $data['b_updated_at'] : new Expression('NOW()');

    }

    /**
     * @param $nameVariable
     * @param $valueVariable
     */
    public function __set( $nameVariable, $valueVariable ) {
        $this->$nameVariable = $valueVariable;
    }

    /**
     * @param $nameVariable
     *
     * @return bool
     */
    public function __get( $nameVariable ) {
        return $this->$nameVariable ? $this->$nameVariable : false;
    }





}