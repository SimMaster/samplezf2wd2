<?php

namespace Book\Model;

use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Di\ServiceLocator;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Renderer\PhpRenderer;

class BookTable {

    /*================================================================================================================*/
    /**
     * Standard functions
     * TODO: move to base extend class
     */
    protected $tableGT;
    protected $rPrefix = "b";

    /**
     * @param TableGateway $tableGT
     */
    public function __construct( TableGateway $tableGT ) {
        $this->tableGT = $tableGT;
    }


    /**
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {

        $objSelect = new Select();
        $objSelect->from( $this->tableGT->table  )
        ;

        $resultSet = $this->tableGT->selectWith( $objSelect );
        return $resultSet;
    }

    /**
     * @param int $id
     *
     * @return array|\ArrayObject|bool|null
     */
    public function getObject($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGT->select(array($this->rPrefix. '_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    /**
     * @param BookItem $object
     *
     * @return bool|int
     */
    public function saveConfiguration(BookItem $object)
    {
        $data = array(

            $this->rPrefix. '_name' => $object->b_name,
            $this->rPrefix. '_published_year' => $object->b_published_year,
            $this->rPrefix. '_price' => floatval($object->b_price),
            $this->rPrefix. '_author' => $object->b_author,
            $this->rPrefix. '_active' => $object->b_active,



        );

        $id = (int)$object->b_id;
        if ($id == 0) {
            $data[$this->rPrefix. '_created_at'] = $object->b_created_at;
            $this->tableGT->insert($data);
            return $this->tableGT->lastInsertValue;
        } else {
            if ($this->getObject($id)) {
                $data[$this->rPrefix. '_updated_at'] = $object->b_updated_at;
                $this->tableGT->update($data, array($this->rPrefix. '_id' => $id));
                return $id;
            } else {
                return false;
            }
        }
    }

    /**
     * @param array $arrParams
     * @param int $id
     *
     * @return bool
     */
    public function updateObjectParams($arrParams, $id)
    {
        if(!is_array($arrParams))
        {
            return false;
        }
        $data = array();
        foreach( $arrParams as $keyString => $strValue)
        {
            $data[$this->rPrefix . '_' . $keyString] = $strValue;
        }
        if ($this->getObject($id)) {
            $data[$this->rPrefix. '_updated_at'] = new Expression('NOW()');
            $this->tableGT->update($data, array($this->rPrefix. '_id' => $id));
            return $id;
        } else {
            return false;
        }

    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function deleteObject($id)
    {
        return $this->tableGT->delete(array($this->rPrefix. '_id' => $id));
    }

    /*
     * End Standard Functions
     */
    /*================================================================================================================*/
    /*
     * Custom functions
     * */


    /**
     * @param bool $flShowAll
     *
     * @return null|\Zend\Db\ResultSet\ResultSetInterface
     */
    public function getList($flShowAll = true)
    {
        $objSelect = new Select();
        $objSelect->from( $this->tableGT->table  )
           /*->join(
                array('links' => 'links_publishing_house_books'),
                $this->tableGT->table . '.' .$this->rPrefix . '_id = links.lphb_b_id',
                '*',
                'left'
            )
            ->join(
                array('ph' => 'publishing_house'),
                'links.lphb_phb_id = ph.pbh_id',
                '*',
                'left'
            )*/

            ->order($this->rPrefix . "_updated_at DESC");
        ;
        if( $flShowAll === false ) {
            $objSelect->where(array( $this->rPrefix."_active = ?" => 1 ));
        }


        $resultSet = $this->tableGT->selectWith( $objSelect );
        return $resultSet;
    }

    /**
     * @param      $objId
     * @param bool $flShowAll
     *
     * @return null|\Zend\Db\ResultSet\ResultSetInterface
     */
    public function getFullInfo($objId, $flShowAll = true)
    {
        $objSelect = new Select();
        $objSelect->from( $this->tableGT->table  )
            /*->join(
                array('links' => 'links_publishing_house_books'),
                $this->tableGT->table . '.' .$this->rPrefix . '_id = links.lphb_b_id',
                '*',
                'left'
            )
            ->join(
                array('ph' => 'publishing_house'),
                'links.lphb_phb_id = ph.pbh_id',
                '*',
                'left'
            )*/
            ->where( $this->rPrefix . '_id = ' . $objId )
            ;
        ;
        if( $flShowAll === false ) {
            $objSelect->where(array( $this->rPrefix."_active = ?" => 1 ));
        }


        $resultSet = $this->tableGT->selectWith( $objSelect );
        return $resultSet;
    }









}