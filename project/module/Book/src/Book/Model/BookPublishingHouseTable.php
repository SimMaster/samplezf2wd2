<?php

namespace Book\Model;

use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Di\ServiceLocator;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Renderer\PhpRenderer;

class BookPublishingHouseTable {

    /*================================================================================================================*/
    /**
     * Standard functions
     * TODO: move to base extend class
     */
    protected $tableGT;
    protected $rPrefix = "pbh";

    /**
     * @param TableGateway $tableGT
     */
    public function __construct( TableGateway $tableGT ) {
        $this->tableGT = $tableGT;
    }


    /**
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {

        $objSelect = new Select();
        $objSelect->from( $this->tableGT->table  )
        ;

        $resultSet = $this->tableGT->selectWith( $objSelect );
        return $resultSet;
    }

    /**
     * @param int $id
     *
     * @return array|\ArrayObject|bool|null
     */
    public function getObject($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGT->select(array($this->rPrefix. '_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    /**
     * @param BookPublishingHouseItem $object
     *
     * @return bool|int
     */
    public function saveConfiguration(BookPublishingHouseItem $object)
    {
        $data = array(
            $this->rPrefix. '_name' => $object->pbh_name,
            $this->rPrefix. '_active' => $object->pbh_active,


        );

        $id = (int)$object->pbh_id;
        if ($id == 0) {
            $data[$this->rPrefix. '_created_at'] = $object->pbh_created_at;
            $this->tableGT->insert($data);
            return $this->tableGT->lastInsertValue;
        } else {
            if ($this->getObject($id)) {
                $data[$this->rPrefix. '_updated_at'] = $object->pbh_updated_at;
                $this->tableGT->update($data, array($this->rPrefix. '_id' => $id));
                return $id;
            } else {
                return false;
            }
        }
    }

    /**
     * @param array $arrParams
     * @param int $id
     *
     * @return bool
     */
    public function updateObjectParams($arrParams, $id)
    {
        if(!is_array($arrParams))
        {
            return false;
        }
        $data = array();
        foreach( $arrParams as $keyString => $strValue)
        {
            $data[$this->rPrefix . '_' . $keyString] = $strValue;
        }
        if ($this->getObject($id)) {
            $data[$this->rPrefix. '_updated_at'] = new Expression('NOW()');
            $this->tableGT->update($data, array($this->rPrefix. '_id' => $id));
            return $id;
        } else {
            return false;
        }

    }

    /**
     * @param int $id
     *
     * @return int
     */
    public function deleteObject($id)
    {
        return $this->tableGT->delete(array($this->rPrefix. '_id' => $id));
    }

    /*
     * End Standard Functions
     */
    /*================================================================================================================*/
    /*
     * Custom functions
     * */

    public function getList()
    {

        $objSelect = new Select();
        $objSelect->from( $this->tableGT->table  )
        ;

        $resultSet = $this->tableGT->selectWith( $objSelect );
        return $resultSet;
    }

    public function getListByArray($arrIds)
    {

        $objSelect = new Select();
        $objSelect->from( $this->tableGT->table  )
            ->where($this->rPrefix . '_id IN (' . implode(',',$arrIds) . ')' )
        ;


        $resultSet = $this->tableGT->selectWith( $objSelect );
        return $resultSet;
    }





}