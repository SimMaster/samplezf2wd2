<?php

namespace Book\Model;

use Zend\Db\Sql\Expression;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class LinkBookPublishingHouseItem  {


    public $lphb_phb_id;
    public $lphb_b_id;



    /**
     * @param array $data
     */
    public function exchangeArray( $data )
    {

        $this->lphb_phb_id = (isset($data['lphb_phb_id'])) ? $data['lphb_phb_id'] : null;
        $this->lphb_b_id = (isset($data['lphb_b_id'])) ? $data['lphb_b_id'] : null;


    }

    /**
     * @param $nameVariable
     * @param $valueVariable
     */
    public function __set( $nameVariable, $valueVariable ) {
        $this->$nameVariable = $valueVariable;
    }

    /**
     * @param $nameVariable
     *
     * @return bool
     */
    public function __get( $nameVariable ) {
        return $this->$nameVariable ? $this->$nameVariable : false;
    }





}