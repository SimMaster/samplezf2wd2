<?php
namespace PbAcl\Controller\Plugin;


use Zend\Mvc\Controller\Plugin\AbstractPlugin,
Zend\Session\Container,
Zend\Permissions\Acl\Acl,
Zend\Permissions\Acl\Role\GenericRole as Role,
Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\Mvc\MvcEvent;

class PbAclPlugin extends AbstractPlugin
{
    protected $sesscontainer ;
    private function getSessContainer()
    {
        if (!$this->sesscontainer) {
            $this->sesscontainer = new Container('userAuth');
        }
        return $this->sesscontainer;
    }

    public function doAuthorization( MvcEvent $e)
    {
        // set ACL
        $acl = new Acl();
        //$acl->deny();
        $acl->allow();

        $acl->addRole(new Role('anonymous'));
        $acl->addRole(new Role('user'),  'anonymous');

        $acl->addRole(new Role('administrator') );


        $acl->addResource('application');
        $acl->addResource('accounts');
        $acl->addResource('book');


        // $acl->allow('role', 'resource', 'controller:action');

        $acl->allow( 'administrator', NULL );
        // Application -------------------------->
        $acl->allow('anonymous', 'application', 'index:index');
        // Accounts
        $acl->allow( 'user', 'accounts', 'auth:index');
        $acl->allow( 'user', 'accounts', 'profile:index');
        $acl->allow( 'user', 'accounts', 'profile');

        $acl->allow( 'anonymous', 'accounts', 'auth:index');
        $acl->allow( 'anonymous', 'accounts', 'auth:logout');
        $acl->allow( 'anonymous', 'accounts', 'auth:register');


        $acl->deny( 'anonymous', 'application', 'admin' );
        $acl->deny( 'anonymous', 'accounts', 'admin' );
        $acl->deny( 'anonymous', 'book', 'admin' );
        $acl->deny( 'anonymous', 'accounts', 'profile' );

        $acl->deny( 'user', 'application', 'admin' );
        $acl->deny( 'user', 'accounts', 'admin' );
        $acl->deny( 'user', 'book', 'admin' );



        $controller = $e->getTarget();
        $controllerClass = get_class($controller);
        $moduleName = strtolower(substr($controllerClass, 0, strpos($controllerClass, '\\')));

        $role = (! $this->getSessContainer()->user_store['ur_name'] ) ? 'anonymous' : $this->getSessContainer()->user_store['ur_name'];
        $routeMatch = $e->getRouteMatch();

        $actionName = strtolower($routeMatch->getParam('action', 'not-found'));
        $controllerName = $routeMatch->getParam('controller', 'not-found');
        $controllerName = strtolower(array_pop(explode('\\', $controllerName)));

        /*print '<br>$moduleName: '.$moduleName.'<br>';
        print '<br>$controllerClass: '.$controllerClass.'<br>';
        print '$controllerName: '.$controllerName.'<br>';
        print '$action: '.$actionName.'<br>';
        print $role.'<br>';
        print $this->getSessContainer()->user_store['u_name'];
        var_dump( ( !$acl->isAllowed( $role, $moduleName, $controllerName.':'.$actionName ) || !$acl->isAllowed( $role, $moduleName, $controllerName ) ) );
        //die('xxx');*/



        if ( !$acl->isAllowed( $role, $moduleName, $controllerName) || !$acl->isAllowed( $role, $moduleName, $controllerName.':'.$actionName ) ) {
                $router = $e->getRouter();
                $url    = $router->assemble( array(), array( 'name' => 'auth' ) ); // assemble a login route
                $response = $e->getResponse();
                $response->setStatusCode(302);
                $response->getHeaders()->addHeaderLine('Location', $url);
                $e->stopPropagation();
        } elseif( $acl->isAllowed( $role, $moduleName, $controllerName) ) {
            if (  !$acl->isAllowed( $role, $moduleName, $controllerName.':'.$actionName )   ){
                $router = $e->getRouter();
                $url    = $router->assemble( array(), array( 'name' => 'auth' ) ); // assemble a login route
                $response = $e->getResponse();
                $response->setStatusCode(302);
                $response->getHeaders()->addHeaderLine('Location', $url);
                $e->stopPropagation();
            }
        }


    }
}