<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Accounts\Factory\Model\UsersTableFactory;
use Ads\Factory\Model\AdsRequestsTableFactory;
use Ads\Factory\Model\ObjectsTableFactory;
use Application\Factory\Model\CacheFactory;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class AdminController extends AbstractActionController
{

    private $fctUsers;
    private $tblUsers;



    protected $userAuth;

    public $cacheFactory;
    public $cacheObject;


    protected $serviceLocator;
    protected $viewModel;






    protected $acceptMapping = array(
        'Zend\View\Model\ViewModel' => array(
            'text/html'
        ),
        'Zend\View\Model\JsonModel' => array(
            'application/json'
        )
    );

    public function onDispatch( MvcEvent $e )
    {



        $this->serviceLocator = $sm = $this->getServiceLocator();

        if ( !$this->tblUsers ) {
            $this->fctUsers = new UsersTableFactory();
            $this->tblUsers = $this->fctUsers->createService( $this->serviceLocator );
        }


        $this->viewModel = $this->acceptableViewModelSelector($this->acceptMapping);

        $sessionAuth = new Container( 'userAuth' );
        $this->userAuth = (object)$sessionAuth->user_store;

        return parent::onDispatch($e);

    }


    public function indexAction()
    {

        return $this->redirect()->toRoute(
            'admin-book',
            array(

            )
        );


    }
}
