<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Book\Factory\Model\BookPublishingHouseTableFactory;
use Book\Factory\Model\BookTableFactory;
use Book\Factory\Model\LinkBookPublishingHouseTableFactory;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    protected $viewModel;

    protected $acceptMapping
        = array(
            'Zend\View\Model\ViewModel' => array(
                'text/html'
            ),
            'Zend\View\Model\JsonModel' => array(
                'application/json'
            )
        );

    protected $userAuth;

    public function onDispatch(MvcEvent $e)
    {
        $this->viewModel = $this->acceptableViewModelSelector($this->acceptMapping);


        $sessionAuth = new Container('userAuth');
        $this->userAuth = (object)$sessionAuth->user_store;

        return parent::onDispatch($e);
    }

    /**
     * @return mixed
     */
    public function indexAction()
    {
        $showError = false;
        $success = false;
        $arrErrors = array();
        $token = "";

        $listBooks = array();
        $listPH = array();

        $fctBooksTable = new BookTableFactory();
        $tblBooks = $fctBooksTable->createService( $this->serviceLocator );
        $listBooks = $tblBooks->getList();
        if( $listBooks ) {
            $fctLinkBooksPH = new LinkBookPublishingHouseTableFactory();
            $tblLinksBooksPH = $fctLinkBooksPH->createService( $this->serviceLocator );

            $fctPHTable = new BookPublishingHouseTableFactory();
            $tblPH = $fctPHTable->createService( $this->serviceLocator );
            $listBooks = $listBooks->toArray();
            foreach( $listBooks as $keyIndex => $objBook ) {
                $selectedPH = array();
                $selectedPH = $tblLinksBooksPH->getListByBook($objBook["b_id"]);

                $listInPH = array();
                if ( count($selectedPH) ) {
                    $listInPH = $tblPH->getListByArray($selectedPH);
                    if($listInPH) {
                        $listInPH = $listInPH->toArray();
                    }

                }

                $listBooks[$keyIndex]["listPH"] = $listInPH;

            }
        }



        $fctPHTable = new BookPublishingHouseTableFactory();
        $tblPH = $fctPHTable->createService( $this->serviceLocator );
        $listPH = $tblPH->getList();


        $arrAnswer['success'] =  $success;
        $arrAnswer['showError'] = $showError;
        $arrAnswer['errors'] = $arrErrors;
        $arrAnswer['token'] = $token;
        $arrAnswer['listBooks'] = $listBooks;
        $arrAnswer['listPH'] = $listPH;

        $this->layout()->setVariable('listPH', $listPH);

        $this->viewModel->setVariables(
            $arrAnswer
        );

        return $this->viewModel;

    }
}
