<?php

/**
 * User: Maxim Morozenko <maxim@morozenko.com>
 * Date: 18.12.13
 * Time: 2:45
 * Version: ${VERSION}
 */


namespace Application\View\Helper;

use Application\Factory\Model\CacheFactory;
use Pages\Factory\Model\BlocksTableFactory;
use Pages\Model\BlocksTable;
use Zend\Di\ServiceLocator;
use Zend\ServiceManager\ServiceManager;
use Zend\Session\Container;
use Zend\View\Helper\AbstractHelper;


class ListNotificationsHelper extends AbstractHelper
{
    protected $notificationsFactoryTable;
    public $listNotifications;

    /*
     *
     */
    public function __construct( ServiceManager $sm ) {

        $this->listNotifications = new ListNotifications();
        //TODO: implement notifications Factory Table Initializations

    }

    public function __invoke( )
    {
        return $this->listNotifications;


    }
}

/**
 * Class ListNotifications
 *
 * @package Application\View\Helper
 */

class ListNotifications {

    public $listWarningMessages;
    public $listInfoMessages;
    public $listTaskMessages;

    public function __construct() {

    }
}