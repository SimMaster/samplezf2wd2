<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }
    public function init ( ModuleManager $manager )
    {
        $events = $manager->getEventManager();

        $sharedEvents = $events->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', function($e) {
                $controller = $e->getTarget();
                $routeMatch = $e->getRouteMatch();
                $controllerName = strtolower(end(explode('\\', $routeMatch->getParam('controller'))));
                $routeName = $routeMatch->getMatchedRouteName();
                if ( $controllerName == 'auth' ) {
                    $controllerName = 'admin';
                }
                $controller->layout('layout/'.$controllerName);
            }, 100);
    }
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'ListBlocksHelper' => function ($sm) {
                    // Get the service locator
                    $serviceLocator = $sm->getServiceLocator();

                    // pass it to your helper
                    return new \Application\View\Helper\ListBlocksHelper( $serviceLocator );
                },
                'ListObjectsHelper' => function ($sm) {
                    // Get the service locator
                    $serviceLocator = $sm->getServiceLocator();

                    // pass it to your helper
                    return new \Application\View\Helper\ListObjectsHelper( $serviceLocator );
                },
                'ListMapAreaHelper' => function ($sm) {
                    // Get the service locator
                    $serviceLocator = $sm->getServiceLocator();

                    // pass it to your helper
                    return new \Application\View\Helper\ListMapAreaHelper( $serviceLocator );
                },
                'ListCategoryAdsHelper' => function ($sm) {
                    // Get the service locator
                    $serviceLocator = $sm->getServiceLocator();

                    // pass it to your helper
                    return new \Application\View\Helper\ListCategoryAdsHelper( $serviceLocator );
                },
                'SearchParamsHelper' => function ($sm) {
                    $serviceLocator = $sm->getServiceLocator();
                    // pass it to your helper

                    return new \Application\View\Helper\SearchParamsHelper( $serviceLocator );
                },

                'ListNotificationsHelper' => function ( $sm ) {
                    $serviceLocator = $sm->getServiceLocator();
                    // pass it to your helper

                    return new \Application\View\Helper\ListNotificationsHelper( $serviceLocator );
                },

            )
        );
    }
}
