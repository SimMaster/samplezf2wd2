<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Accounts\Controller\Auth' => 'Accounts\Controller\AuthController',
            'Accounts\Controller\Admin' => 'Accounts\Controller\AdminController',

        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'accounts' => __DIR__ . '/../view',
        ),
    ),
    'router' => array(
        'routes' => array(

            'auth' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/auth[/]',

                    'defaults' => array(
                        'controller' => 'Accounts\Controller\Auth',
                        'action'     => 'index',
                    ),
                ),
            ),
            'logout' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/logout[/]',

                    'defaults' => array(
                        'controller' => 'Accounts\Controller\Auth',
                        'action'     => 'logout',
                    ),
                ),
            ),



        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'accounts' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy'
        ),
    ),
    'view_helpers' => array(
        'invokables'=> array(
            'userAuthHelper' => 'Accounts\View\Helper\UserAuthHelper'
        )
    ),

);