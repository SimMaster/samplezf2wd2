<?php

namespace Accounts\View\Helper;
use Zend\Session\Container;
use Zend\View\Helper\AbstractHelper;

class UserAuthHelper extends AbstractHelper
{
    /*
     *
     */
    public function __invoke( $name, $value = null )
    {
        $sessionAuth = new Container( 'userAuth' );

        if (isset($sessionAuth->user_store[$name])) {
            if ( !$value ) {
                return $sessionAuth->user_store[$name];
            } else {
                return $sessionAuth->user_store[$name] = $value;
            }
        }

    }
}