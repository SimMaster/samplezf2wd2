<?php

namespace Accounts\Model;

use Zend\Db\Sql\Expression;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class User implements InputFilterAwareInterface {

    public $u_id;
    public $u_name;
    public $u_active;
    public $u_link_restore;
    public $u_role_id;


    public  $u_created_at;
    public  $u_updated_at;
    public $u_datetime_restore;



    public $ur_name;
    public $ur_description;

    public  $u_passwd;
    public  $u_st;
    public  $u_code_active;

    public $u_not_remove;
    public $u_not_edit;

    public $u_auth_code;

    public $u_cp_link;
    public $u_restore_link;

    protected $inputFilter;


    /**
     * @param array $data
     */
    public function exchangeArray( $data )
    {

        $this->u_id = (isset($data['u_id'])) ? $data['u_id'] : null;
        $this->u_name = (isset($data['u_name'])) ? $data['u_name'] : null;
        $this->u_active = (isset($data['u_active'])) ? $data['u_active'] : 0;
        $this->u_link_restore = (isset($data['u_link_restore'])) ? $data['u_link_restore'] : null;
        $this->u_role_id = (isset($data['u_role_id'])) ? $data['u_role_id'] : 4;

        $this->ur_name = (isset($data['ur_name'])) ? $data['ur_name'] : null;
        $this->ur_description = (isset($data['ur_description'])) ? $data['ur_description'] : null;

        $this->u_created_at = (isset($data['u_created_at'])) ? $data['u_created_at'] : new Expression('NOW()');
        $this->u_updated_at = (isset($data['u_updated_at'])) ? $data['u_updated_at'] : new Expression('NOW()');
        $this->u_code_active = (isset($data['u_code_active'])) ? $data['u_code_active'] : '';
        $this->u_datetime_restore = (isset($data['u_datetime_restore'])) ? $data['u_datetime_restore'] : null;


        $this->u_st = (isset($data['u_st'])) ? $data['u_st'] : null ;
        $this->u_passwd = (isset($data['u_passwd'])) ? $data['u_passwd'] :  null ;

        $this->u_not_remove = (isset($data['u_not_remove'])) ? $data['u_not_remove'] :  0 ;
        $this->u_not_edit = (isset($data['u_not_edit'])) ? $data['u_not_edit'] :  0 ;

        $this->u_auth_code = (isset($data['u_auth_code'])) ? $data['u_auth_code'] : null;

        $this->u_cp_link = (isset($data['u_auth_code'])) ? $data['u_auth_code'] : null;
        $this->u_restore_link = (isset($data['u_auth_code'])) ? $data['u_auth_code'] : null;
    }

    /**
     * @param $nameVariable
     * @param $valueVariable
     */
    public function __set( $nameVariable, $valueVariable ) {
        $this->$nameVariable = $valueVariable;
    }

    /**
     * @param $nameVariable
     *
     * @return bool
     */
    public function __get( $nameVariable ) {
        return $this->$nameVariable ? $this->$nameVariable : false;
    }

    // Add content to this method:
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }


    public function getInputFilter()
    {
        if ( !$this->inputFilter ) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();

            $inputFilter->add(
                $factory->createInput(
                    array(
                         'name'     => 'u_id',
                         'required' => true,
                         'filters'  => array(
                             array('name' => 'Int'),
                         ),
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                         'name'     => 'u_name',
                         'required' => true,
                         'filters'  => array(
                             array('name' => 'StripTags'),
                             array('name' => 'StringTrim'),
                         ),
                         'validators' => array(
                             array(
                                 'name' => 'not_empty',
                             ),
                             array(
                                 'name' => 'email_address',
                             ),

                         ),
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(

                         'name'     => 'u_role_id',
                         'required' => true,
                         'filters'  => array(
                             array('name' => 'Int'),
                             array('name' => 'StringTrim'),
                         ),
                         'validators' => array(
                             array(
                                 'name' => 'not_empty',
                             ),


                         ),
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                         'name'     => 'u_passwd',
                         'required' => true,
                         'filters'  => array(
                             array('name' => 'StripTags'),
                             array('name' => 'StringTrim'),
                         ),
                         'validators' => array(
                             array(
                                 'name' => 'not_empty',
                             ),
                             array(
                                 'name' => 'string_length',
                                 'options' => array(
                                     'min' => 8
                                 ),
                             ),
                         ),
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }


}