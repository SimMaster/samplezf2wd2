<?php

namespace Accounts\Model;

use Zend\Db\Sql\Expression;

class UserRole {

    public $ur_id;
    public $ur_name;
    public $ur_description;
    public $ur_created_at;
    public $ur_updated_at;

    /**
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->ur_id            = (isset($data['ur_id'])) ? $data['ur_id'] : null;
        $this->ur_name          = (isset($data['ur_name'])) ? $data['ur_name'] : null;
        $this->ur_description   = (isset($data['ur_description'])) ? $data['ur_description'] : null;
        $this->ur_created_at    = (isset($data['ur_created_at'])) ? $data['ur_created_at'] : new Expression('NOW()');
        $this->ur_updated_at    = (isset($data['ur_updated_at'])) ? $data['ur_updated_at'] : new Expression('NOW()');
    }
}