<?php

namespace Accounts\Model;

use Zend\Db\TableGateway\TableGateway;

class UserRolesTable {

    protected $tableGT;

    /**
     * @param TableGateway $tableGT
     */
    public function __construct( TableGateway $tableGT ) {
        $this->tableGT = $tableGT;
    }


    /**
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        $resultSet = $this->tableGT->select();
        return $resultSet;
    }

    /**
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getListRolesForChange()
    {
        $rowset = $this->tableGT->select(array('ur_ct_change' => 1));

        return $rowset;
    }

    /**
     * @param int $ur_id
     *
     * @return array|\ArrayObject|null
     * @throws \Exception
     */
    public function getUserRole( $ur_id )
    {
        $ur_id  = (int) $ur_id;
        $rowset = $this->tableGT->select(array('ur_id' => $ur_id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $ur_id");
        }
        return $row;
    }

    /**
     * @param UserRole $objUserRole
     *
     * @return int
     * @throws \Exception
     */
    public function saveUserRole( UserRole $objUserRole )
    {

        $data = array(
            'ur_name' => $objUserRole->ur_name,
            'ur_description'  =>  $objUserRole->ur_description,

        );

        $ur_id = (int)$objUserRole->ur_id;
        if ($ur_id == 0) {
            $data['ur_created_at'] = $objUserRole->ur_created_at;
            return $this->tableGT->insert($data);
        } else {
            if ($this->getUserRole( $ur_id ) ) {
                $data['ur_updated_at'] = $objUserRole->ur_updated_at;
                return $this->tableGT->update( $data, array('ur_id' => $ur_id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    /**
     * @param int $ur_id
     *
     * @return int
     */
    public function deleteUser( $ur_id )
    {
        return $this->tableGT->delete(array('ur_id' => $ur_id));
    }



}