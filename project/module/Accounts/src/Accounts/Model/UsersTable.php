<?php

namespace Accounts\Model;

use Mailer\Factory\Model\MailerStackTableFactory;
use Mailer\Factory\Model\MailerTableFactory;
use Mailer\Model\MailerItem;
use Mailer\Model\MailerStackTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Di\ServiceLocator;
use Zend\ModuleManager\Listener\ServiceListenerInterface;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceManager;

class UsersTable {

    protected $tableGT;

    /**
     * @param TableGateway $tableGT
     */
    public function __construct( TableGateway $tableGT ) {
        $this->tableGT = $tableGT;
    }


    /**
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {

        $objSelect = new Select();
        $objSelect->from( $this->tableGT->table  )
            ->join(
                array( 'roles' => 'users_role' ),
                $this->tableGT->table . '.u_role_id = roles.ur_id',
                '*',
                'left'
            );


        $select = $objSelect;
        $resultSetPrototype = new ResultSet();


        $paginatorAdapter = new DbSelect(
        // our configured select object
            $select,
            // the adapter to run it against
            $this->tableGT->getAdapter(),
            // the result set to hydrate
            $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;


    }




    /**
     * @param int $u_id
     *
     * @return array|\ArrayObject|null
     * @throws \Exception
     */
    public function getUser( $u_id )
    {
        $u_id  = (int) $u_id;
        $rowset = $this->tableGT->select(array('u_id' => $u_id));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    /**
     * @param string $u_name
     *
     * @return array|\ArrayObject|null
     * @throws \Exception
     */
    public function getUserByName( $u_name )
    {

        $objSelect = new Select();
        $objSelect->from( $this->tableGT->table  )
            ->join(
                array( 'roles' => 'users_role' ),
                $this->tableGT->table . '.u_role_id = roles.ur_id',
                '*',
                'left'
            )->where(  array( 'u_name' => $u_name ) );


        $rowSet = $this->tableGT->selectWith( $objSelect );
        $row = $rowSet->current();
        if (!$row) {
           return false;
        }

        return $row;
    }

    /**
     * @param $u_id
     *
     * @return mixed
     * @throws \Exception
     */
    public function getUserByNameId( $u_id )
    {

        $objSelect = new Select();
        $objSelect->from( $this->tableGT->table  )
            ->join(
                array( 'roles' => 'users_role' ),
                $this->tableGT->table . '.u_role_id = roles.ur_id',
                '*',
                'left'
            )->where(  array( 'u_id' => $u_id ) );


        $rowSet = $this->tableGT->selectWith( $objSelect );
        $row = $rowSet->current();
        if (!$row) {
            return false;
        }

        return $row;
    }


    /**
     * @param string $u_name
     * @param string $pwd
     *
     * @return bool
     * @throws \Exception
     */
    public function checkUserPassword( $u_name, $pwd ) {
        if ( !$u_name || !$pwd ) {
            throw new \Exception('Variables must be set.');
        }

        $objUser = $this->getUserByName( $u_name );
        if ( !$objUser ) {
            return false;
        }

        $strPwdForCheck = md5( $objUser->u_st . $pwd );
        if ( $strPwdForCheck === $objUser->u_passwd && ( $objUser->u_active == 1 ) ) {
            return true;
        } else {
            if ( !$objUser->u_active ) {
                return -3;
            } else {
                return false;
            }
        }

    }





    /**
     * @param int $u_id
     *
     * @return bool|mixed
     */
    public function getUserFullObject( $u_id ) {

        if ( !$u_id ) {
            return false;
        }

        $sql = new Sql( $this->tableGT->adapter );
        $objSelect = $sql->select(  );
        $objSelect->from( array( 'user' => $this->tableGT->table ) )
            ->join(
                array( 'role' => 'users_role' ),
                'user.u_role_id = role.ur_id',
                '*',
                'left'
            )
            ->where( 'user.u_id = ' . $u_id );
        $statement = $sql->prepareStatementForSqlObject($objSelect);
        $result = $statement->execute();

        $objUser = $result->current();
        if ( !$objUser ) {
            return false;
        }

        return $objUser;
    }




}