<?php

namespace Accounts\Factory\Model;


use Accounts\Model\User;
use Accounts\Model\UsersTable;
use Mailer\Model\MailerItem;
use Mailer\Model\MailerStackTable;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;

use Zend\Stdlib\Hydrator\ObjectProperty;
use Zend\Db\ResultSet\HydratingResultSet;

class UsersTableFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $db = $serviceLocator->get('Zend\Db\Adapter\Adapter');

        $resultSetPrototype = new HydratingResultSet();
        $resultSetPrototype->setHydrator(new ObjectProperty());
        $resultSetPrototype->setObjectPrototype(new User());

        $tableGateway       = new TableGateway('users', $db, null, $resultSetPrototype);
        $table              = new UsersTable($tableGateway);

        return $table;
    }

}

