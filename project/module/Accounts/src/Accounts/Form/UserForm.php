<?php
namespace Accounts\Form;


use Accounts\Model\UserRolesTable;
use Zend\Form\Form;

class UserForm extends Form
{

    protected $userRolesTable;

    public function __construct( $name = null, UserRolesTable $tblUserRolesTable )
    {
        // we want to ignore the name passed

        $this->userRolesTable = $tblUserRolesTable;

        $listUserRoles = $this->userRolesTable->fetchAll();

        $selectListsUserRoles = $this->formatUserRolesList( $listUserRoles );

        parent::__construct('user');
        $this->setAttribute('meth1od', 'post');

        $this->add(
            array(
                    'name' => 'u_id',
                    'attributes' => array(
                        'type'  => 'hidden',
                    ),
               )
        );
        $this->add(
            array(
                    'name' => 'u_name',
                    'attributes' => array(
                        'type'  => 'text',
                    ),
                    'options' => array(
                        'label' => 'E-mail',
                    ),
               )
        );
        $this->add(
            array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'u_role_id',
                    'options' => array(
                        'label' => 'Select user role',
                        'value_options' => $selectListsUserRoles,
                    )
                )
        );
        $this->add(
            array(
                 'name' => 'u_name',
                 'attributes' => array(
                     'type'  => 'text',
                 ),
                 'options' => array(
                     'label' => 'E-mail',
                 ),
            )
        );
        $this->add(
            array(
                'name' => 'u_passwd',
                'attributes' => array(
                    'type'  => 'password',
                ),
                'options' => array(
                    'label' => 'Password',
                ),
           )
        );
        $this->add(
            array(
                    'name' => 'submit',
                    'attributes' => array(
                        'type'  => 'submit',
                        'value' => 'Go',
                        'id' => 'submitbutton',
                    ),
               )
        );
    }

    /**
     * @param $listUserRoles
     *
     * @return array
     */
    protected function formatUserRolesList( $listUserRoles ) {

        if ( !$listUserRoles ) {
            return array();
        }
        $arrUserRoles = array();

        foreach( $listUserRoles as $keyIndex => $objUserRole ) {
            $arrUserRoles[ $objUserRole->ur_id ] = $objUserRole->ur_name;
        }


        return $arrUserRoles;
    }

    /**
     * @param $nameVariable
     * @param $valueVariable
     */
    public function __set( $nameVariable, $valueVariable ) {
        $this->$nameVariable = $valueVariable;
    }

    /**
     * @param $nameVariable
     *
     * @return bool
     */
    public function __get( $nameVariable ) {
        return $this->$nameVariable ? $this->$nameVariable : false;
    }



}