<?php

namespace Accounts\Controller;


use Accounts\Factory\Model\UsersTableFactory;
use Accounts\Model\AccountUser;
use Accounts\Model\User;
use Mailer\Factory\Model\MailerStackTableFactory;
use Mailer\Model\MailerItem;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Zend\View\Helper\ServerUrl;
use Zend\View\Model\ViewModel;



class AuthController extends AbstractActionController
{
    protected $usersTable;
    protected $viewModel;

    protected $acceptMapping = array(
        'Zend\View\Model\ViewModel' => array(
            'text/html'
        ),
        'Zend\View\Model\JsonModel' => array(
            'application/json'
        )
    );


    protected $userAuth;

    public function onDispatch( MvcEvent $e )
    {
        $this->serviceLocator = $sm = $this->getServiceLocator();

        $this->viewModel = $this->acceptableViewModelSelector($this->acceptMapping);
        $sessionAuth = new Container( 'userAuth' );
        $this->userAuth = (object)$sessionAuth->user_store;

        //+TODO: remove before debugging
        return parent::onDispatch($e);

    }


    public function indexAction()
    {
        $this->layout('layout/login');
        $showError = false;
        $loginStatus = 'invalid';
        $redirectUrl = '';
        $arrForm = $this->params()->fromPost( 'login', null );

        $sessionAuth = new Container('userAuth');

        //die(var_dump($sessionAuth->user_store));
        if ( isset($sessionAuth->user_store) ) {
            if ( $sessionAuth->user_store['ur_name'] != DEF_LOGIN_ANONYMOUS_NAME_ROLE && $sessionAuth->user_store['ur_name'] != NULL ) {
                if ( $sessionAuth->user_store['ur_name'] == DEF_ADMIN_NAME_ROLE ) {
                    return $this->redirect()->toRoute('admin');
                } else {
                    return $this->redirect()->toRoute('home');
                }
            }
        }

        if ( !is_array( $arrForm ) ) {
            $showError = true;
        }
        $userName = ( isset($arrForm['name'])? $arrForm['name'] : null );
        $userPwd = ( isset($arrForm['pwd'])? $arrForm['pwd'] : null );

        if ( !$userName || !$userPwd ) {
            $showError = true;
        }



        $sm = $this->getServiceLocator();
        $this->usersTable = $sm->get('Accounts\Model\UsersTable');
        $viewModel = $this->acceptableViewModelSelector($this->acceptMapping);
        $fctUsersTable = new UsersTableFactory();
        $tblUsers = $fctUsersTable->createService( $this->serviceLocator );
        $answerAuth = '';
        if ( !$showError ) {

            $answerAuth = $tblUsers->checkUserPassword( $userName, $userPwd );

            if ( $answerAuth === true ) {
                $objUser = $tblUsers->getUserByName( $userName );

                if ( $objUser ) {

                    $sessionAuth->user_store = array();
                    $sessionAuth->user_store['u_id'] =  $objUser->u_id;
                    $sessionAuth->user_store['u_name'] =  $objUser->u_name;
                    $sessionAuth->user_store['u_role_id'] =  $objUser->u_role_id;
                    $sessionAuth->user_store['ur_name'] = $objUser->ur_name;

                    $loginStatus = 'success';
                    $link = $this->url()->fromRoute('admin', array());
                    $helper = new ServerUrl();
                    $baseUrl = $helper->getScheme() . '://' . $helper->getHost();
                    $redirectUrl = $baseUrl . $link ;
                    //return $this->redirect()->toRoute('home');

                }
            } elseif ( $answerAuth === -3 ) {
                $showError = true;
                $loginStatus = 'not_active';
            } elseif ( $answerAuth === false ) {
                $showError = true;

            }


        }

        //or use ViewHelperManager in controller or other place that you have ServiceManager
       // $link = $this->getServiceLocator()->get('ViewHelperManager')->get('url')->__invoke('admin',array());


        $viewModel->setVariables(
            array(
                  'redirect_url' => $redirectUrl,
                  'login_status' => $loginStatus,
                  'answerAuth' => $answerAuth,
                  'showError' => $showError
             )
        );


        return $viewModel;
    }


    public function logoutAction() {

        $showError = false;
        $viewModel = $this->acceptableViewModelSelector($this->acceptMapping);
        $viewModel->setTerminal( true );
        $answerUnAuth = '';


        $sessionAuth = new Container('userAuth');
        $sessionAuth->user_store = array();
        $sessionAuth->user_store['u_id'] =  null;
        $sessionAuth->user_store['u_name'] =  null;
        $sessionAuth->user_store['u_role_id'] =  null;
        $sessionAuth->user_store['ur_name'] = null;
        $answerUnAuth = true;
        //die($this->getRequest()->isXmlHttpRequest());
        if ( !$this->getRequest()->isXmlHttpRequest() ) {
            return $this->redirect()->toRoute('home');
        }

        $viewModel->setVariables(
            array(
                 'answerUnAuth' => $answerUnAuth,
                 'showError' => $showError
            )
        );


        return $viewModel;
    }





}

