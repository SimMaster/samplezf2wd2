<?php

namespace Accounts\Controller;

use Accounts\Factory\Model\AccountsUsersTableFactory;
use Accounts\Factory\Model\UsersRolesTableFactory;
use Accounts\Factory\Model\UsersTableFactory;
use Accounts\Model\AccountUser;
use Application\Factory\Model\CacheFactory;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

use Accounts\Model\User;
use Accounts\Form\UserForm;

class AdminController extends AbstractActionController
{

    protected $usersTableFactory;
    protected $usersRolesTableFactory;
    protected $accountsUsersTableFactory;

    protected $usersTable;
    protected $usersRolesTable;

    protected $accountsUsersTable;

    protected $sessionMessage;
    protected $objSessionMessageAdmin;

    protected $acceptMapping = array(
        'Zend\View\Model\ViewModel' => array(
            'text/html'
        ),
        'Zend\View\Model\JsonModel' => array(
            'application/json'
        )
    );

    public function onDispatch( MvcEvent $e )
    {

        $this->serviceLocator = $sm = $this->getServiceLocator();

        $this->usersTableFactory = new UsersTableFactory();
        $this->usersRolesTableFactory = new UsersRolesTableFactory();
        $this->accountsUsersTableFactory = new AccountsUsersTableFactory();

        $this->usersTable = $this->usersTableFactory->createService( $this->serviceLocator );
        $this->usersRolesTable = $this->usersRolesTableFactory->createService( $this->serviceLocator );
        $this->accountsUsersTable = $this->accountsUsersTableFactory->createService( $this->serviceLocator );


        $this->cacheFactory = new CacheFactory( $sm );
        $this->cacheObject = $this->cacheFactory->createService( $sm );
        $this->viewModel = $this->acceptableViewModelSelector($this->acceptMapping);

        if ( !$this->sessionMessage ) {
            $this->objSessionMessageAdmin = new Container('messageAdmin');
            $this->sessionMessage =  array(
                'error' => isset($this->objSessionMessageAdmin->adminMessage['error'])?$this->objSessionMessageAdmin->adminMessage['error']:null,
                'message' => isset($this->objSessionMessageAdmin->adminMessage['message'])?$this->objSessionMessageAdmin->adminMessage['message']:null,
            );
        }


        return parent::onDispatch($e);

    }



    public function indexAction()
    {

        return $this->redirect()->toRoute(
            'admin-book',
            array(

            )
        );


    }



    protected function _cleanSessionMessage() {
        if ( isset($this->objSessionMessageAdmin->adminMessage) ) {
            unset($this->objSessionMessageAdmin->adminMessage);
        }
        return 1;
    }



}

