<?php
namespace Accounts;

use Accounts\Model\User;
use Accounts\Model\UsersTable;
use Accounts\Model\UserRole;
use Accounts\Model\UserRolesTable;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\ModuleManager;

class Module
{
    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function init ( ModuleManager $manager )
    {
        $events = $manager->getEventManager();

        $sharedEvents = $events->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', function($e) {
                $controller = $e->getTarget();
                $routeMatch = $e->getRouteMatch();
                $controllerName = strtolower(end(explode('\\', $routeMatch->getParam('controller'))));
                $routeName = $routeMatch->getMatchedRouteName();
                if ( $controllerName == 'auth' ) {
                    $controllerName = 'admin';
                }
                $controller->layout('layout/'.$controllerName);
            }, 100);
    }
    /**
     * @return array
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Accounts\Model\UsersTable' =>  function($sm) {
                    $tableGateway = $sm->get('UsersTableGateway');
                    $table = new UsersTable( $tableGateway );
                    return $table;
                },
                'UsersTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype( new User() );

                    return new TableGateway( 'users', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounts\Model\UserRolesTable' =>  function($sm) {
                        $tableGateway = $sm->get('UserRolesTableGateway');
                        $table = new UserRolesTable( $tableGateway );
                        return $table;
                    },
                'UserRolesTableGateway' => function ($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $resultSetPrototype->setArrayObjectPrototype( new UserRole() );

                        return new TableGateway( 'users_role', $dbAdapter, null, $resultSetPrototype);
                    },
                'Accounts\Model\UsersTableOut' =>  'Accounts\Factory\Model\UsersTableFactory',



            ),
        );
    }

    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'userAuthHelper' => function($sm) {
                        $helper = new View\Helper\UserAuthHelper;
                        return $helper;
                    }
            )
        );
    }
}
